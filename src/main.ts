import { App, CfnOutput, Stack, StackProps } from 'aws-cdk-lib';
import { LambdaRestApi } from 'aws-cdk-lib/aws-apigateway';
import { Runtime } from 'aws-cdk-lib/aws-lambda';
import { NodejsFunction } from 'aws-cdk-lib/aws-lambda-nodejs';
import { Construct } from 'constructs';

export class GreetingStack extends Stack {
  public readonly greetingEndpoint: CfnOutput;

  constructor(scope: Construct, id: string, props: StackProps = {}) {
    super(scope, id, props);

    const greetingHandler = new NodejsFunction(this, 'GreetingHandler', {
      runtime: Runtime.NODEJS_20_X,
      entry: 'src/handlers/greeting-handler.ts',
      depsLockFilePath: 'yarn.lock',
    });

    const gateway = new LambdaRestApi(this, 'GreetingGateway', {
      handler: greetingHandler,
    });

    this.greetingEndpoint = new CfnOutput(this, 'GreetingGatewayEndpoint', {
      value: gateway.url,
    });

  }
}

// for development, use account/region from cdk cli
const devEnv = {
  account: process.env.CDK_DEFAULT_ACCOUNT,
  region: process.env.CDK_DEFAULT_REGION,
};

const app = new App();

new GreetingStack(app, 'gitlab-migration-poc-dev', { env: devEnv });
// new MyStack(app, 'gitlab-migration-poc-prod', { env: prodEnv });

app.synth();