import { APIGatewayProxyResult, APIGatewayEvent } from 'aws-lambda';
import { greeting } from '../core/greeting';

export const handler = async (_: APIGatewayEvent): Promise<APIGatewayProxyResult> => ({
  statusCode: 200,
  body: JSON.stringify({
    message: greeting(),
  }),
});