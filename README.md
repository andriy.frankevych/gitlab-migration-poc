# GitLab Migration Test Project

[![pipeline status](https://gitlab.com/andriy.frankevych/gitlab-migration-poc/badges/main/pipeline.svg)](https://gitlab.com/andriy.frankevych/gitlab-migration-poc/-/commits/main)
[![coverage report](https://gitlab.com/andriy.frankevych/gitlab-migration-poc/badges/main/coverage.svg)](https://gitlab.com/andriy.frankevych/gitlab-migration-poc/-/commits/main)
[![Quality Gate Status](https://sonarqube.engineering.docebo.io/api/project_badges/measure?project=andriy.frankevych_gitlab-migration-poc_AY2NcA6fvMwIvyn4OGy2&metric=alert_status&token=sqb_86b5865715517f6da38c72317b2fc406d2091794)](https://sonarqube.engineering.docebo.io/dashboard?id=andriy.frankevych_gitlab-migration-poc_AY2NcA6fvMwIvyn4OGy2)

A sample project to test GitLab integration with AWS CodePipeline and Sonarqube
