const {awscdk, gitlab} = require('projen');
const {AllowFailure, JobWhen} = require("projen/lib/gitlab/configuration-model");

const NODE_VERSION = '20.11.0';

const project = new awscdk.AwsCdkTypeScriptApp({
  cdkVersion: '2.1.0',
  defaultReleaseBranch: 'main',
  name: 'gitlab-migration-poc',
  minNodeVersion: `v${NODE_VERSION}`,
  maxNodeVersion: `v${NODE_VERSION}`,
  deps: ['@types/aws-lambda'],
  // description: undefined,  /* The description is just a string that helps people understand the purpose of the package. */
  // devDeps: [],
  // packageName: undefined,  /* The "name" in package.json. */
});


new gitlab.GitlabConfiguration(project,
  {
    default: {
      image: {
        name: `node:${NODE_VERSION}`,
      },
      beforeScript: ['npm install'],
      cache: [
        {
          paths: ['node_modules/'],
        }
      ],
    },
    jobs: {
      synth: {
        stage: 'build',
        script: ['npm run synth'],
      },
      test: {
        stage: 'build',
        script: ['npm run test'],
        coverage: '/All files[^|]*\\|[^|]*\\s+([\\d\\.]+)/',
        artifacts: {
          paths: ['coverage/*'],
          reports: {
            coverageReport: {
              coverageFormat: 'cobertura',
              path: 'coverage/cobertura-coverage.xml',
            },
          },
        },
      },
      lint: {
        stage: 'build',
        script: ['npm run eslint'],
      },
      // TODO find how to set GIT_DEPTH: "0" in sonar check
      'sonarqube-check': {
        stage: 'build',
        image: {
          name: 'sonarsource/sonar-scanner-cli:latest',
          entrypoint: [''],
        },
        variables: {
          'SONAR_USER_HOME': '${CI_PROJECT_DIR}/.sonar'
        },
        cache: [
          {
            key: '${CI_JOB_NAME}',
            paths: ['.sonar/cache']
          }
        ],
        beforeScript: [''],
        script: ['sonar-scanner'],
        allowFailure: true,
        rules: [
          {
            if: "$CI_COMMIT_BRANCH == 'main'"
          }
        ]
      }
    },
  });

project.synth();