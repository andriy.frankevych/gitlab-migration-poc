import { greeting } from '../../../src/core/greeting';

test('it greets', () => {
  expect(greeting()).toStrictEqual('Hello World!');
});